#include <irrlicht.h>
#include "museumMain.h"

using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;


/* Main application code. */
int main(int argc, char** argv) {
	// create device
	irr::IrrlichtDevice* device = irr::createDevice(irr::video::EDT_OPENGL,irr::core::dimension2d<irr::u32>(1280,720),32,false,true,false);
	if (device == 0)
       	return 1;

    museumMain* museum = new museumMain(device);

	/* Mainloop */
	museum->render();

	/* Cleanup */
	museum->stop();

	return 0;
}
