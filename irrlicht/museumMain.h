#ifndef MUSEUMMAIN_H
#define MUSEUMMAIN_H

#include <irrlicht.h>

class museumMain
{
    public:
        museumMain(irr::IrrlichtDevice* device);
        virtual ~museumMain();

        void setup(irr::IrrlichtDevice* device);
		void render();
		void stop();

		/* Return Irrlicht pointers */
        irr::gui::ICursorControl* getCursor();
		irr::scene::ISceneManager* getSceneManager();
		irr::gui::IGUIEnvironment* getGUIEnvironment();
		irr::video::IVideoDriver* getVideoDriver();
		irr::s32 getRealTime();

    protected:
    private:
        irr::u32 timeLastTick;
        irr::u32 timeNowTick;

        irr::IrrlichtDevice* device;
        irr::video::SColor* backgroundColor;

        //museumEventReceiver* eventReceiver;

        irr::scene::ICameraSceneNode* camera;

        //Collision
        //gameCollision* collision;
};

#endif // MUSEUMMAIN_H
