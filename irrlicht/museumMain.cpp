#include "museumMain.h"

museumMain::museumMain(irr::IrrlichtDevice* device) {
    setup(device);
    timeLastTick=0;
}

museumMain::~museumMain() {
    stop();
}

void museumMain::render() {
    irr::u32 cSavePos=0;

    while(device->run()) {
        timeNowTick=device->getTimer()->getTime();

        //Only if we're on the current window
        if ((timeNowTick-timeLastTick)>16) {
            if (device->isWindowActive()) {

                //eventReceiver->processEvents(camera, player, mapVillage, &gameItems, collision, playerInventory, (timeNowTick-timeLastTick));

                //render
                getVideoDriver()->beginScene(true, true, *backgroundColor);


                //Render game
                getSceneManager()->drawAll();

                 //FPS
                int fps = getVideoDriver()->getFPS();
                irr::core::stringw strFPS = L"FPS: ";
                strFPS += fps;
                strFPS += L" Triangles: ";
                strFPS += getVideoDriver()->getPrimitiveCountDrawn();
                device->setWindowCaption(strFPS.c_str());

                getVideoDriver()->endScene();
            }
            timeLastTick=timeNowTick;
        }
        device->yield();
    }
}

void museumMain::setup(irr::IrrlichtDevice* pDevice) {
    device = pDevice;

    //Initiate timers
    timeLastTick=device->getTimer()->getTime();
    timeNowTick=timeLastTick;

    /* BACKGROUND COLOR */
    backgroundColor = new irr::video::SColor(255,224,224,224);

    /* EVENT RECEIVER */
    //eventReceiver = new gameEventReceiver(gameGUIManager);
    //device->setEventReceiver(eventReceiver);

    /* CAMERA */
    camera = getSceneManager()->addCameraSceneNodeFPS();

    /* TESTS ITEMS */
    irr::scene::IMeshSceneNode* floor = this->getSceneManager()->addCubeSceneNode(1.0f,0,-1,irr::core::vector3df(0.f),irr::core::vector3df(0.f),irr::core::vector3df(256.0f,1.0f,256.0f));

    /* SKYDOME */
    getSceneManager()->addSkyDomeSceneNode(getVideoDriver()->getTexture("media/skydome.jpg"));

    /* LIGHT */
    getSceneManager()->setAmbientLight(irr::video::SColorf(.5f,.5f,.5f,1));
    getSceneManager()->addLightSceneNode(0,irr::core::vector3df(0,256.0f,0),irr::video::SColorf(1.0f,1.0f,1.0f),256.0f);

    /* PHYSICS */
    //collision = new gameCollision(this->getSceneManager());
    //collision->initCollisionItemsList(&gameItems);
    //collision->initCollisions();

    /* DONE */
}

/* Remove objects and closes program */
void museumMain::stop() {
    device->setEventReceiver(0);
	device->closeDevice();
	device->drop();
}

irr::gui::ICursorControl* museumMain::getCursor() {
    return device->getCursorControl();
}

irr::scene::ISceneManager* museumMain::getSceneManager() {
    return device->getSceneManager();
}

irr::gui::IGUIEnvironment* museumMain::getGUIEnvironment() {
    return device->getGUIEnvironment();
}

irr::video::IVideoDriver* museumMain::getVideoDriver() {
    return device->getVideoDriver();
}

irr::s32 museumMain::getRealTime() {
    return device->getTimer()->getRealTime();
}
