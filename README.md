# **3DCG Museum** #

**How to contribute**

Contact admins to be added to the team. Alternatively, just upload to Dropbox or something and existing members will push.

**What to contribute**

Models in .obj format, with textures in .png or .tif format.

**Needed models**

Pretty much everything, the museum building itself, maybe some exterior stuff, and ofcourse models to be on display.